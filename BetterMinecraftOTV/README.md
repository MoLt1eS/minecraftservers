# BetterMinecraftOTV

# Client

## Requirements

Mod cliend of choise, I use [MultiMC](https://multimc.org/) you can also use Curse client or other alternatives

Required

- MOD PACK: https://www.curseforge.com/minecraft/modpacks/better-minecraft-otv-edition
- OPTIFINE: https://optifine.net/adloadx?f=OptiFine_1.16.5_HD_U_G8.jar&x=f9bf
- CREATE MOD: https://www.curseforge.com/minecraft/mc-mods/create/files/3278516
- Cucumber: https://www.curseforge.com/minecraft/mc-mods/cucumber/files/3349690
- Mystical Agriculture: https://www.curseforge.com/minecraft/mc-mods/mystical-agriculture/files/3321276
- Botany Pots: https://www.curseforge.com/minecraft/mc-mods/botany-pots/files/3283196

Optional

- SHADER SEUS: https://sonicether.com/shaders/download/renewed-v1-0-1/
- RESOURCE PACK: NAPP


## Instructions

1. Install the mod pack, this will create a modded minecraft instance
1. Add `optifine`, `create`, `cucumber` and `mystical agriculture` jar files to the `/mods` directory inside the created modded minecraft instance
1. (Optional) add the shader to the `/shaderpacks` directory and the resource pack to `/resourcepacks`


# Server

# Intructions

Add https://www.curseforge.com/minecraft/mc-mods/netherportalfix to the mods folder
